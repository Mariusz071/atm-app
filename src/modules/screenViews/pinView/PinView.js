import React, { Component } from "react";
import UserContext from "context/UserContext";
import { pinViewText } from "data/dictionary";
import "./PinView.scss";

class PinView extends Component {
  render() {
    const { pin } = this.props;
    const { pinFails, pinTries } = this.context;

    const placeholder = pin.split("");
    const failedPin = pinFails > 0;

    return (
      <div className="pin-view">
        <h2>Enter your PIN</h2>
        {failedPin && <h3>{pinViewText[pinTries]}</h3>}
        <div className="pin-view__placeholder">
          {placeholder.map((char, key) => (
            <div key={key} className="pin-view__placeholder-char">
              X
            </div>
          ))}
        </div>
      </div>
    );
  }
}

PinView.contextType = UserContext;
export default PinView;
