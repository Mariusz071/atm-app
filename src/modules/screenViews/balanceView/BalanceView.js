import React from "react";

import RedirectButton from "modules/common/RedirectButton";

import "./BalanceView.scss";
const BalanceView = props => (
  <div className="balance-view">
    <RedirectButton label="Back to options" url="/options" />
    <h2>Your account balance is {props.balance}$.</h2>
  </div>
);

export default BalanceView;
