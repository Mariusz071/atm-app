import React, { Component } from "react";

import UserContext from "context/UserContext";

class DepositView extends Component {
  state = {
    amount: ""
  };

  onAmountChange = amount => {
    this.setState({ amount });
  };

  render() {
    const { keypadValue } = this.props;

    return (
      <div className="deposit-view">
        <h2>Deposit view</h2>
        <div>
          <h3>{keypadValue}</h3>
        </div>
      </div>
    );
  }
}

export default DepositView;
