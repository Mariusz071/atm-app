import React from "react";

const BlockedView = () => (
  <div className="screen">
    <h2>Your card has been blocked. Please contact your bank.</h2>
  </div>
);

export default BlockedView;
