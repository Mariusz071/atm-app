import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./OptionsView.scss";
class OptionsView extends Component {
  options = [
    {
      label: "Balance",
      path: "/balance"
    },
    {
      label: "Withdraw",
      path: "/actions/withdraw/initial"
    },
    {
      label: "Deposit",
      path: "/actions/deposit"
    },
    {
      label: "Statement",
      path: "/statement"
    },
    {
      label: "Pin change",
      path: "/pin-change"
    },
    {
      label: "Bank services",
      path: "/services"
    }
  ];

  render() {
    return (
      <div className="options-view">
        {this.options.map((option, key) => (
          <Link className="options-view__option" key={key} to={option.path}>
            {option.label}
          </Link>
        ))}
      </div>
    );
  }
}

export default OptionsView;
