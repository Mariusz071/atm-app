import React, { Component } from "react";

import RedirectButton from "modules/common/RedirectButton";
import WithdrawViewInitial from "./WithdrawView.initial";
import WithdrawViewCompleted from "./WithdrawView.completed";
import LoadingView from "../../common/loadingView/LoadingView";
import UserContext from "context/UserContext";
import { wait } from "modules/common/helpers";
// import history from "modules/common/history";
import "./WithdrawView.scss";

class WithdrawView extends Component {
  state = {
    amount: 0,
    loading: false
  };

  onCashWithdraw = async amount => {
    const { onBalanceChange } = this.context;
    const { history } = this.props;
    this.setState({ loading: true, amount }, () =>
      onBalanceChange(this.state.amount, "withdraw")
    );
    await wait(1000);
    this.setState({ loading: false }, () =>
      history.push("/withdraw/completed")
    );
    await wait(2000);
    history.push("/options");
  };

  render() {
    const { loading } = this.state;
    const {
      match: { params },
      keypadValue
    } = this.props;
    return (
      <div className="withdraw-view">
        <RedirectButton label="Back to options" url="/options" />
        {loading && <LoadingView />}
        {params.view === "completed" && <WithdrawViewCompleted />}
        {!loading && params.view === "initial" && (
          <WithdrawViewInitial
            onCashWithdraw={this.onCashWithdraw}
            keypadValue={keypadValue}
          />
        )}
      </div>
    );
  }
}
WithdrawView.contextType = UserContext;
export default WithdrawView;
