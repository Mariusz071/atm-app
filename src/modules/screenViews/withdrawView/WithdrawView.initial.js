import React, { Component } from "react";

import "./WithdrawView.scss";

class WithdrawViewInitial extends Component {
  state = {
    amount: 0
  };

  notesButtons = [
    [
      {
        label: "10",
        value: 10
      },
      {
        label: "20",
        value: 20
      },
      {
        label: "50",
        value: 50
      },
      {
        label: "100",
        value: 100
      }
    ],
    [
      {
        label: "200",
        value: 200
      },
      {
        label: "500",
        value: 500
      },
      {
        label: "1000",
        value: 1000
      },
      {
        label: "2000",
        value: 2000
      }
    ]
  ];
  render() {
    const { onCashWithdraw, keypadValue } = this.props;

    return (
      <div>
        <h2>Select an amount.</h2>
        <div className="withdraw-view__buttons">
          {this.notesButtons.map((col, i) => (
            <div key={i} className="withdraw-view__column">
              {col.map((button, j) => (
                <button
                  key={j}
                  value={button.value}
                  onClick={() => onCashWithdraw(button.value)}
                >
                  {button.label}
                </button>
              ))}
            </div>
          ))}
        </div>
        <div className="withdraw-view__enter">
          <h2>Enter an amount to withdraw.</h2>
          <div className="withdraw-view__amount">{keypadValue}</div>
        </div>
      </div>
    );
  }
}

export default WithdrawViewInitial;
