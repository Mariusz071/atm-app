import React from "react";

const WithdrawViewCompleted = () => (
  <div>
    <h2>Please take your cash promptly</h2>
  </div>
);

export default WithdrawViewCompleted;
