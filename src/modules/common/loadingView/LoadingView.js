import React from "react";
import LoadingIcon from "modules/icons/Loading";

const LoadingView = () => (
  <div>
    <h3>Your request is being processed.</h3>
    <LoadingIcon />
  </div>
);

export default LoadingView;
