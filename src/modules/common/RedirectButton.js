import React from "react";
import history from "modules/common/history";

const RedirectButton = props => (
  <button onClick={() => history.push(props.url)}>{props.label}</button>
);

export default RedirectButton;
