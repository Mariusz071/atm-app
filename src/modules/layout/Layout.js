import React, { Component } from "react";
import Atm from "../atm";

import "./Layout.scss";

class Layout extends Component {
  render() {
    return (
      <div className="layout">
        <Atm />
      </div>
    );
  }
}

export default Layout;
