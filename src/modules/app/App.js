import React from "react";
import { Router } from "react-router-dom";
import history from "modules/common/history";
import Layout from "../layout";
import Atm from "../atm";
import { UserStore } from "../../context/UserContext";

import "./App.scss";

function App() {
  return (
    <div className="App">
      <Router history={history}>
        <UserStore>
          <Layout>
            <Atm />
          </Layout>
        </UserStore>
      </Router>
    </div>
  );
}

export default App;
