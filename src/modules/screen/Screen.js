import React, { Component } from "react";
import { Route } from "react-router-dom";

import history from "modules/common/history";
import UserContext from "context/UserContext";
import BlockedView from "modules/screenViews/blockedView/BlockedView";
import OptionsView from "../screenViews/optionsView/OptionsView";
import PinView from "../screenViews/pinView/PinView";
import BalanceView from "../screenViews/balanceView/BalanceView";
import WithdrawView from "../screenViews/withdrawView/WithdrawView";
import DepositView from "../screenViews/depositView/DepositView";
import LoadingIcon from "modules/icons/Loading";

import "./Screen.scss";

class Screen extends Component {
  render() {
    const { keypadValue, loading } = this.props;
    const { user } = this.context;
    if (this.context.isLogged === false) history.push("/");
    return (
      <div className="screen">
        {loading ? (
          <LoadingIcon />
        ) : (
          <>
            <Route
              exact
              path="/"
              render={props => <PinView pin={keypadValue} {...props} />}
            />
            <Route exact path="/options" component={OptionsView} />
            <Route exact path="/blocked" component={BlockedView} />
            <Route
              exact
              path="/balance"
              render={props => (
                <BalanceView balance={user.balance} {...props} />
              )}
            />
            <Route
              exact
              path="/actions/:action/:view"
              render={props => (
                <WithdrawView keypadValue={keypadValue} {...props} />
              )}
            />
            <Route
              exact
              path="/actions/:action"
              render={props => (
                <DepositView keypadValue={keypadValue} {...props} />
              )}
            />
          </>
        )}
      </div>
    );
  }
}

Screen.contextType = UserContext;
export default Screen;
