import React, { Component } from "react";
import mapKeys from "lodash.mapkeys";

import UserContext from "context/UserContext";
import { wait } from "modules/common/helpers";
import history from "modules/common/history";
import mockDatabase from "data/db";

import "./Keypad.scss";

class KeyPad extends Component {
  state = {
    inputValue: "",
    loading: false
  };

  getValue = e => {
    let { inputValue } = this.state;
    const { getKeypadValue } = this.props;
    const { value } = e.target;

    inputValue += value;
    this.setState({ inputValue }, () => getKeypadValue(this.state.inputValue));
  };

  signIn = async () => {
    const { onLoad } = this.props;
    const { onPinFail } = this.context;
    const { getKeypadValue } = this.props;
    this.setState({ loading: true }, () => onLoad(this.state.loading));
    await wait(300);
    this.setState({ loading: false }, () => onLoad(this.state.loading));
    this.getUser();

    if (!this.context.user) {
      onPinFail();
      this.setState({ inputValue: "" }, () =>
        getKeypadValue(this.state.inputValue)
      );

      if (this.context.blocked === true) return history.push("/blocked");
      return history.push("/");
    }

    this.setState({ inputValue: "" }, () =>
      getKeypadValue(this.state.inputValue)
    );
    history.push("/options");
  };

  // acceptAmount = () => {
  //   console.log(this.props);
  //   const { onBalanceChange } = this.context;
  //   const { inputValue } = this.state;

  //   onBalanceChange(inputValue, params.action);
  //   history.push("/balance");
  // };

  cancel = () => {
    const { signOut } = this.context;
    const { getKeypadValue } = this.props;

    this.setState({ inputValue: null }, () =>
      getKeypadValue(this.state.inputValue)
    );

    history.push("/");
    signOut();
  };

  clear = () => {
    const { inputValue } = this.state;
    const { getKeypadValue } = this.props;
    const newValue = inputValue.slice(0, -1);

    this.setState({ inputValue: newValue }, () =>
      getKeypadValue(this.state.inputValue)
    );
  };

  getUser = () => {
    const { signIn } = this.context;
    const { inputValue } = this.state;
    const users = mapKeys(mockDatabase, "pin");
    const user = users[inputValue];
    signIn(user);
  };

  buttons = () => {
    const isPinView = history.location.pathname === "/";

    return [
      [
        {
          value: "1",
          label: "1",
          onClick: this.getValue
        },
        {
          value: "2",
          label: "2",
          onClick: this.getValue
        },
        {
          value: "3",
          label: "3",
          onClick: this.getValue
        },
        {
          label: "Cancel",
          onClick: this.cancel
        }
      ],

      [
        {
          value: "4",
          label: "4",
          onClick: this.getValue
        },
        {
          value: "5",
          label: "5",
          onClick: this.getValue
        },
        {
          value: "6",
          label: "6",
          onClick: this.getValue
        },
        {
          label: "Clear",
          onClick: this.clear
        }
      ],
      [
        {
          value: "7",
          label: "7",
          onClick: this.getValue
        },
        {
          value: "8",
          label: "8",
          onClick: this.getValue
        },
        {
          value: "9",
          label: "9",
          onClick: this.getValue
        },
        {
          label: "Enter",
          onClick: isPinView ? this.signIn : this.acceptAmount
        }
      ],
      [
        {
          label: "",
          disabled: true
        },
        {
          label: "0",
          value: "0",
          onClick: this.getValue
        },
        {
          label: "",
          disabled: true
        },
        {
          label: "",
          disabled: true
        }
      ]
    ];
  };
  render() {
    return (
      <div className="keypad">
        {this.buttons().map((row, i) => {
          return (
            <div className="keypad__row" key={i}>
              {row.map((button, j) => (
                <button
                  disabled={button.disabled}
                  key={j}
                  className="btn-keypad"
                  onClick={button.onClick}
                  value={button.value}
                >
                  {button.label}
                </button>
              ))}
            </div>
          );
        })}
      </div>
    );
  }
}

KeyPad.contextType = UserContext;
export default KeyPad;
