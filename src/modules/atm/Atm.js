import React, { Component } from "react";

import UserContext from "context/UserContext";

import Screen from "../screen";
import KeyPad from "../keypad";

import "./Atm.scss";
class Atm extends Component {
  state = {
    keypadValue: "",
    loading: false
  };

  getKeypadValue = keypadValue => {
    this.setState({ keypadValue });
  };

  onLoad = loading => {
    this.setState({ loading });
  };

  balanceUpdate = (func, type) => {
    func(type);
  };

  render() {
    const { keypadValue, loading } = this.state;
    return (
      <div className="atm">
        <Screen keypadValue={keypadValue} loading={loading} />
        <KeyPad getKeypadValue={this.getKeypadValue} onLoad={this.onLoad} />
      </div>
    );
  }
}

Atm.contextType = UserContext;
export default Atm;
