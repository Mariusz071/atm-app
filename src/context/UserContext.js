import React, { Component } from "react";

const Context = React.createContext({ user: {}, isLogged: false });

export class UserStore extends Component {
  state = {
    user: null,
    pinFails: 0,
    pinTries: 3,
    blocked: false
  };

  signIn = user => {
    this.setState({ user });
  };

  signOut = () => {
    this.setState({ user: {} });
  };

  onPinFail = () => {
    let { pinFails, pinTries } = this.state;
    pinFails++;
    pinTries--;
    this.setState({ pinFails, pinTries });

    if (pinFails === 3) {
      this.setState({ blocked: true });
    }
  };

  onBalanceChange = (amount, type) => {
    const balance = this.state.user.balance;

    amount = parseInt(amount);
    let newBalance;

    if (type === "deposit") {
      newBalance = balance + amount;
    } else if (type === "withdraw") {
      newBalance = balance - amount;
    }
    this.setState(prevState => {
      return {
        user: {
          ...prevState.user,
          balance: newBalance
        }
      };
    });
  };

  render() {
    return (
      <Context.Provider
        value={{
          ...this.state,
          signIn: this.signIn,
          signOut: this.signOut,
          onPinFail: this.onPinFail,
          onBalanceChange: this.onBalanceChange,
          isLogged: !!this.state.user,
          block: this.block
        }}
      >
        {this.props.children}
      </Context.Provider>
    );
  }
}

export default Context;
